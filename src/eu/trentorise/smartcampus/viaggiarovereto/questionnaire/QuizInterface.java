package eu.trentorise.smartcampus.viaggiarovereto.questionnaire;

public interface QuizInterface {
	public void nextQuestion();
}
